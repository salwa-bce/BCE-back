<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231016103923 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE attestation_formation (attestation_id INT NOT NULL, formation_id INT NOT NULL, INDEX IDX_A56425277EDC5B38 (attestation_id), INDEX IDX_A56425275200282E (formation_id), PRIMARY KEY(attestation_id, formation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attestation_formation ADD CONSTRAINT FK_A56425277EDC5B38 FOREIGN KEY (attestation_id) REFERENCES attestation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE attestation_formation ADD CONSTRAINT FK_A56425275200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BF7EDC5B38');
        $this->addSql('DROP INDEX IDX_404021BF7EDC5B38 ON formation');
        $this->addSql('ALTER TABLE formation DROP attestation_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE attestation_formation DROP FOREIGN KEY FK_A56425277EDC5B38');
        $this->addSql('ALTER TABLE attestation_formation DROP FOREIGN KEY FK_A56425275200282E');
        $this->addSql('DROP TABLE attestation_formation');
        $this->addSql('ALTER TABLE formation ADD attestation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF7EDC5B38 FOREIGN KEY (attestation_id) REFERENCES attestation (id)');
        $this->addSql('CREATE INDEX IDX_404021BF7EDC5B38 ON formation (attestation_id)');
    }
}
