<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231016133006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, etudiant_id INT NOT NULL, module_id INT DEFAULT NULL, attestation_id INT DEFAULT NULL, client VARCHAR(255) NOT NULL, date_completion DATE DEFAULT NULL, status TINYINT(1) DEFAULT NULL, INDEX IDX_404021BFDDEAB1A3 (etudiant_id), INDEX IDX_404021BFAFC2B591 (module_id), INDEX IDX_404021BF7EDC5B38 (attestation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE module (id INT AUTO_INCREMENT NOT NULL, title LONGTEXT NOT NULL, credit DOUBLE PRECISION NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFDDEAB1A3 FOREIGN KEY (etudiant_id) REFERENCES etudiant (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BF7EDC5B38 FOREIGN KEY (attestation_id) REFERENCES attestation (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BFDDEAB1A3');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BFAFC2B591');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BF7EDC5B38');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE module');
    }
}
