<?php
// src/Security/AuthenticationSuccessHandler.php

namespace App\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AuthenticationSuccessHandler implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_authentication_success' => ['onAuthenticationSuccess', 10],
        ];
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        /** @var JWTAuthenticationSuccessResponse $response */
        $response = $event->getResponse();
        $data = $event->getData();

        /** @var UserInterface $user */
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        // This is the place to add your own data to the JWT response
        $data['user'] = [
            'id' => $user->getId(), // Assuming the user entity has a method getId()
            'username' => $user->getName(),
            'email' => $user->getEmail(),
            // ... add more fields as needed
        ];

        $event->setData($data);
    }
}
