<?php
namespace App\Service;

use App\Entity\Attestation;
use App\Entity\Etudiant;
use App\Entity\Formation;
use Doctrine\ORM\EntityManagerInterface;

class AttestationService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function generateAttestations(Etudiant $etudiant): void
    {
        $query = $this->entityManager->createQuery('
            SELECT
                f,
                MONTH(f.dateCompletion) as month,
                YEAR(f.dateCompletion) as year
            FROM App\Entity\Formation f
            WHERE f.etudiant = :etudiant
            AND f.dateCompletion BETWEEN :startDate AND :endDate
            GROUP BY year, month
        ')
        ->setParameter('etudiant', $etudiant)
        ->setParameter('startDate', (new \DateTimeImmutable())->modify('-30 days'))
        ->setParameter('endDate', new \DateTimeImmutable());

        $groupedFormations = $query->getResult();
        var_dump($groupedFormations);
        die;
        foreach ($groupedFormations as $group) {
            $attestation = new Attestation();
            $attestation->setEtudiant($etudiant);
            $attestation->setStatus(true);
            $attestation->setCreatedAt(new \DateTimeImmutable());

            foreach ($group['f'] as $formation) {
                $attestation->addFormation($formation);
            }

            // If you generate a PDF or any file for the attestation, you can set its path.
            // $path = $this->generateAttestationPDF($attestation);
            // $attestation->setPath($path);

            $this->entityManager->persist($attestation);
        }

        $this->entityManager->flush();
    }

    // Optionally, you can add a method here to generate a PDF for the attestation and return its path.
}
