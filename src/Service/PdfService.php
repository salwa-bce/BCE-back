<?php
namespace App\Service;
use TCPDF;

class PdfService
{
    public function createPdfFromImageTemplate($templateImagePath, $anotherTemplateImagePath,$data, $outputPath)
    {
        $width = 297;
        $height = 210;
    
        // Création de l'objet PDF
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, 'mm',  [$width, $height], true, 'UTF-8', false);
    
        // Désactiver l'en-tête et le pied de page
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false, 0);
        // Définir les marges à zéro pour éviter tout espace inattendu
        $pdf->SetMargins(0, 0, 0, true);
    
        // Ajouter une nouvelle page avec les dimensions personnalisées
        $pdf->AddPage('L', [$width, $height]);
    
        // Ajouter le gabarit sous forme d'image avec mode FullScreen pour couvrir toute la page
        $pdf->Image($templateImagePath, 0, 0, $width, $height, '', '', '', false, 300, '', false, false, 0, false, false, false);
    
        // Ajoutez du texte ou d'autres éléments
          // Ajouter le texte 'user' avec une police Times, en italique, 14pt
          $pdf->SetTextColor(129, 129, 129);
          $pdf->SetFont('helvetica', 'B', 18);
          $pdf->Text(26, 76, $data['user']);
    // Ajouter le texte 'client' avec une police Helvetica, normale, 12pt
            $pdf->SetTextColor(147, 144, 144 );
            $pdf->SetFont('times', 'I', 14);
            $pdf->Text(27, 88, $data['client']);

          

            // Ajouter le texte 'credit' avec une police Arial, en gras, 10pt
            $pdf->SetTextColor(162, 159, 159);
            $pdf->SetFont('helvetica', 'b', 10);
            $pdf->Text(63, 148.5, $data['credit']);

            // Ajouter le texte 'date' avec une police Courier, normale, 12pt
            $pdf->SetTextColor(85, 101, 139 );
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Text(189, 88, $data['date']);

            $pdf->AddPage('L', [$width, $height]);

            // Ajouter un autre gabarit (ou le même) sous forme d'image pour la deuxième page
            $pdf->Image($anotherTemplateImagePath, 0, 0, $width, $height, '', '', '', false, 300, '', false, false, 0, false, false, false);
            // Ajouter du texte ou d'autres éléments à la deuxième page
            $pdf->SetTextColor(85, 101, 139);
            $pdf->SetFont('helvetica', '', 9);
        //     $pdf->Text(30, 50, 'Sterile - M0 - Hand hygiene and garbing-Practical (1st year)');
        //     $pdf->Text(30, 55, 'Sterile - M1,M1.1, M1.2, M1.4, M1.6 and pharmaceutical calculations - Theoretical (1st year)');
        //     $pdf->Text(30, 60, 'Sterile - M3.2 - Hazardous compounding sterile preparations  - Theoretical (1st year)');
        //     $pdf->Text(30, 65, 'Sterile - M3.1 - Specific safety measures for handling hazardous drugs - Theoretical and practical (1st year)');
          $x=30;
           $y=50;
             foreach ($data['modules'] as $module) {
                $pdf->Text($x, $y, $module);
                $y+=5;
            }
        // Générer le PDF
        $pdf->Output($outputPath, 'F');  // Sauvegarde le PDF localement
       // $pdf->Output($outputPath, 'I');  // Affiche le PDF dans le navigateur
    }
    
    
    
    
}

