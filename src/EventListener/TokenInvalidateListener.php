<?php
namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class TokenInvalidateListener
{
    private $tokenStorage;
    private $userProvider;

    public function __construct($tokenStorage, $userProvider)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userProvider = $userProvider;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $token = $event->getRequest()->headers->get('Authorization');
        if (!$token) {
            return;
        }

        // Assuming you have a method that checks whether a token is invalidated
        if ($this->isTokenInvalidated($token)) {
            $event->setResponse(new JsonResponse(['error' => 'Token invalidated'], 401));
        }
    }

    private function isTokenInvalidated($token)
    {
        // Your logic here. For example, check against a database if this token has been invalidated
        // If you're using the secret change method, you'll check if the token can be verified 
        // with the current secret of the user

        $username = $this->tokenStorage->getToken()->getUsername();
        $user = $this->userProvider->loadUserByUsername($username);

        try {
            // Your JWT library method to decode and verify the token
            // If this fails, the token has been invalidated
            $decoded = JWT::decode($token, 'your_global_secret' . $user->getSecret(), ['HS256']);
            return false; // Token is valid
        } catch (\Exception $e) {
            return true; // Token is invalidated
        }
    }
}
