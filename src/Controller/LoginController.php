<?php
// src/Controller/LoginController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class LoginController extends AbstractController
{
    public function login(UserInterface $user = null): JsonResponse
    {
        // If the user isn't authenticated
    
    }

   
    public function logout(): JsonResponse
    {
        // This method can remain empty, as you will instruct the client to remove the token on their side.
        return new JsonResponse(['message' => 'Logout successful']);
    }
}
