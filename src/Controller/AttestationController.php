<?php
// src/Controller/AttestationController.php

namespace App\Controller;

use App\Entity\Attestation;
use App\Entity\Formation;
use App\Repository\AttestationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\PdfService;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class AttestationController extends AbstractController
{

    private $pdfService;

    public function __construct(PdfService $pdfService)
    {
        $this->pdfService = $pdfService;
    }
    public function list(AttestationRepository $repository, Request $request)
    {
        $page = $request->query->getInt('page', 1); // Default to the first page if not set
        $limit = 20; // Number of records per page. Adjust as needed.

        $queryBuilder = $repository->createQueryBuilder('a')
            ->join('a.etudiant', 'e') // Join with Etudiant entity
            ->join('a.formations', 'f') // Join with Formations from Attestation
            // Assuming Formation has a relation to Module
            ->select(
                'distinct a.id',
                'a.client',
                'e.fullName as user',
                'a.credit',
                'a.dateFormations', // Assuming Formation has a dateCompletion field
                'a.modules as modules',
                'a.status as etat',
                'a.path',
                'a.createdAt'
                // Assuming Module has a title field
            )
            ->orderBy('a.createdAt', 'DESC'); // Order by dateCompletion in descending order

        // Calculate the total number of results
        $totalItems = count($queryBuilder->getQuery()->getResult());


        // Apply pagination
        $queryBuilder
            ->setFirstResult($limit * ($page - 1)) // Set the offset for the query
            ->setMaxResults($limit); // Limit the number of records

        $attestations = $queryBuilder->getQuery()->getResult();
        //var_dump( $attestations); die();
        $pagesCount = ceil($totalItems / $limit);

        // Include pagination meta-data if desired
        $response = [
            'attestations' => $attestations,
            'totalItems' => $totalItems,
            'currentPage' => $page,
            'pagesCount' => $pagesCount
        ];

        return $this->json($response);
    }

    public function show(int $id, AttestationRepository $repository, EntityManagerInterface $em, SerializerInterface $serializer): Response
    {
        $attestation = $repository->find($id);

        if (!$attestation) {
            return $this->json(['error' => 'Attestation not found'], 404);
        }

        $formations = [];
        foreach ($attestation->getFormations() as $formation) {
            $formations[] = ['module' => $formation->getModule()->getTitle()];
        }

        $data = [
            'id' => $attestation->getId(),
            'client' => $attestation->getClient(),
            'user' => $attestation->getEtudiant()->getFullName(),
            'modules' => $formations,
            'credit' => $attestation->getCredit(),
            'date' => $attestation->getCreatedAt()->format('Y-m-d')
        ];


        return $this->json(['data' => $data], 200);
    }


    public function generatePdf(int $id, Request $request, AttestationRepository $repository, EntityManagerInterface $em)
    {

        $attestation = $repository->find($id);

        if (!$attestation) {
            return $this->json(['error' => 'Attestation not found'], 404);
        }

        $modules = $attestation->getModules();


        $data = [
            'id' => $attestation->getId(),
            'client' => $attestation->getClient(),
            'user' => $attestation->getEtudiant()->getFullName(),
            'modules' => $modules,
            'credit' => $attestation->getCredit(),
            'date' => $attestation->getCreatedAt()->format('F d, Y'),
            'date-attest' => $attestation->getCreatedAt()->format('Y-m-d'),
        ];
        $attest_name = $data['user'] . '_' . $data['date-attest'];
        $projectDir = $this->getParameter('kernel.project_dir');
        $templateImagePath = 'pdf/page-0.jpg'; // Chemin de l'image du gabarit
        $anotherTemplateImagePath = 'pdf/page_2.jpg';  // Le texte à ajouter sur le PDF
        $outputPath = $projectDir . '/public/uploads/attestation_' . $attest_name . '.pdf';
        $path = '/uploads/attestation_' . $attest_name . '.pdf';
        // Chemin du nouveau PDF

        $attestation = $em->getRepository(Attestation::class)->find($id);
        if ($attestation) { // Ensure the formation was fetched successfully
            $attestation->setStatus(true);
            $attestation->setPath($path);
        }
        $em->flush();
        $this->pdfService->createPdfFromImageTemplate($templateImagePath, $anotherTemplateImagePath, $data, $outputPath);
        $response = [
            'data ' => $outputPath];
        
       // $response->headers->set('Access-Control-Allow-Origin', '*');
       return $this->json($response);
    }
    public function pdflist(AttestationRepository $repository, Request $request)
    {
        $page = $request->query->getInt('page', 1); // Default to the first page if not set
        $limit = 10; // Number of records per page. Adjust as needed.

        $queryBuilder = $repository->createQueryBuilder('a')
            ->select(
                'distinct a.id',
                'a.client',
                'e.fullName as user',
                'a.dateFormations',
                'a.status as etat',
                'a.path',
                'a.createdAt'
            )
            ->leftJoin('a.etudiant', 'e')  // Assuming you have a relation with Etudiant, join it to get the fullName
            ->where('a.path IS NOT NULL') // This line ensures that attestations with a non-null path are retrieved
            ->orderBy('a.createdAt', 'DESC'); // Order by dateCompletion in descending order

        // Calculate the total number of results
        $totalItems = count($queryBuilder->getQuery()->getResult());


        // Apply pagination
        $queryBuilder
            ->setFirstResult($limit * ($page - 1)) // Set the offset for the query
            ->setMaxResults($limit); // Limit the number of records

        $attestations = $queryBuilder->getQuery()->getResult();
        //var_dump( $attestations); die();
        $pagesCount = ceil($totalItems / $limit);

        // Include pagination meta-data if desired
        $response = [
            'documents' => $attestations,
            'totalItems' => $totalItems,
            'currentPage' => $page,
            'pagesCount' => $pagesCount
        ];

        return $this->json($response);
    }
}
