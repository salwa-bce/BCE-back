<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Etudiant;
use App\Repository\EtudiantRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class EtudiantApiController extends AbstractController
{
   
    public function index(EtudiantRepository $etudiantRepository, EntityManagerInterface $em, Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1); // Default to the first page if not set
        $limit = 10; // Number of records per page. Adjust as needed.
    
        $query = $etudiantRepository
            ->createQueryBuilder('e')
            ->orderBy('e.fullName', 'ASC') // Order by fullName in ascending order
            ->getQuery();
    
        $paginator = new Paginator($query);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Set the offset for the query
            ->setMaxResults($limit); // Limit the number of records
    
        $data = [];
        foreach ($paginator as $etudiant) {
            $data[] = [
                'id' => $etudiant->getId(),
                'fullName' => $etudiant->getFullName(),
            ];
        }
    
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / $limit);
    
        // Include pagination meta-data if you like
        $response = [
            'etudiants' => $data,
            'totalItems' => $totalItems,
            'currentPage' => $page,
            'pagesCount' => $pagesCount
        ];
    
        return new JsonResponse($response);
    }
    
    
 
    public function show(int $id,EntityManagerInterface $em): JsonResponse
    {
        $etudiant = $em->getRepository(Etudiant::class)->find($id);
        if (!$etudiant) {
            return new JsonResponse(['message' => 'Etudiant not found'], Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse([
            'id' => $etudiant->getId(),
            'fullName' => $etudiant->getFullName()
        ]);
    }


    public function create(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $etudiant = new Etudiant();
        $etudiant->setFullName($data['fullName']);
        $em->persist($etudiant);
        $em->flush();
        return new JsonResponse(['message' => 'Etudiant created'], Response::HTTP_CREATED);
    }
   
    public function update(int $id, Request $request,EntityManagerInterface $em): JsonResponse
    {
        $etudiant = $em->getRepository(Etudiant::class)->find($id);
        if (!$etudiant) {
            return new JsonResponse(['message' => 'Etudiant not found'], Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);
        $etudiant->setFullName($data['fullName']);
      $em->flush();
        return new JsonResponse(['message' => 'Etudiant updated']);
    }

   

    public function delete(int $id,EntityManagerInterface $em): JsonResponse
    {
        $etudiant = $em->getRepository(Etudiant::class)->find($id);
        if (!$etudiant) {
            return new JsonResponse(['message' => 'Etudiant not found'], Response::HTTP_NOT_FOUND);
        }

        $em->remove($etudiant);
        $em->flush();
        return new JsonResponse(['message' => 'Etudiant deleted']);
    }
}
