<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use App\Entity\Formation;
use App\Entity\Etudiant;
use App\Entity\Module;
use App\Repository\FormationRepository;


class FormationApiController extends AbstractController
{
   
    public function index(EntityManagerInterface $em, Request $request): JsonResponse
{
    $page = $request->query->getInt('page', 1); // Default to the first page if not set
    $limit = 20; // Number of records per page. Adjust as needed.

    $query = $em->getRepository(Formation::class)
        ->createQueryBuilder('f')
        ->orderBy('f.dateCompletion', 'DESC') // Order by dateCompletion in descending order
        ->getQuery();

    $paginator = new Paginator($query);
    $paginator->getQuery()
        ->setFirstResult($limit * ($page - 1)) // Set the offset for the query
        ->setMaxResults($limit); // Limit the number of records

    $data = [];
    foreach ($paginator as $formation) {
        $data[] = [
            'id' => $formation->getId(),
            'client' => $formation->getClient(),
            'etudiant' => $formation->getEtudiant()?->getFullName(),
            'dateCompletion' => $formation->getDateCompletion()?->format('Y-m-d'),
            'module' => $formation->getModule()?->getTitle(),
            'credit' => $formation->getModule()?->getCredit()
        ];
    }

    $totalItems = count($paginator);
    $pagesCount = ceil($totalItems / $limit);

    // Include pagination meta-data if you like
    $response = [
        'formations' => $data,
        'totalItems' => $totalItems,
        'currentPage' => $page,
        'pagesCount' => $pagesCount
    ];

    return new JsonResponse($response);
}
    
    
 
    public function show(int $id,EntityManagerInterface $em): JsonResponse
    {
        $formation = $em->getRepository(Formation::class)->find($id);
        if (!$formation) {
            return new JsonResponse(['message' => 'Formation not found'], Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse([
            'id' => $formation->getId(),
            'client' => $formation->getClient(),
            'etudiant_id' => $formation->getEtudiant()?->getId(),
            'dateCompletion' => $formation->getDateCompletion()?->format('Y-m-d'),
            'module_id' => $formation->getModule()?->getId()
        ]);
    }


    public function create(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $formation = new Formation();
        // You need to fetch the Etudiant and Module entities based on the IDs sent in the request.
        $etudiant = $em->getRepository(Etudiant::class)->find($data['etudiant_id']);
        $module = $em->getRepository(Module::class)->find($data['module_id']);
        
        $formation->setClient($data['client'])
                  ->setEtudiant($etudiant)
                  ->setDateCompletion(new \DateTime($data['dateCompletion']))
                  ->setModule($module);
                  
    
        $em->persist($formation);
        $em->flush();
        return new JsonResponse(['message' => 'Formation created'], Response::HTTP_CREATED);
    }
   
    public function update(int $id, Request $request,EntityManagerInterface $em): JsonResponse
    {
        $formation = $em->getRepository(Formation::class)->find($id);
        if (!$formation) {
            return new JsonResponse(['message' => 'Formation not found'], Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);
        $etudiant = $em->getRepository(Etudiant::class)->find($data['etudiant_id']);
        $module = $em->getRepository(Module::class)->find($data['module_id']);
        
        $formation->setClient($data['client'])
                  ->setEtudiant($etudiant)
                  ->setDateCompletion(new \DateTime($data['dateCompletion']))
                  ->setModule($module);
                  
        $em->flush();
        return new JsonResponse(['message' => 'Formation updated']);
    }

   

    public function delete(int $id,EntityManagerInterface $em): JsonResponse
    {
        $formation = $em->getRepository(Formation::class)->find($id);
        if (!$formation) {
            return new JsonResponse(['message' => 'Formation not found'], Response::HTTP_NOT_FOUND);
        }
       
        $em->remove($formation);
        $em->flush();
        return new JsonResponse(['message' => 'Formation deleted']);
    }
}
