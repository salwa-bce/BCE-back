<?php

// src/Controller/ApiExelController.php

namespace App\Controller;

use App\Entity\Attestation;
use App\Entity\Formation;
use App\Entity\Etudiant;
use App\Entity\Module;
use App\Form\ExcelImportType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Psr\Log\LoggerInterface;


class ApiExelController extends AbstractController
{
    private $logger;
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory, LoggerInterface $logger)
    {
        $this->formFactory = $formFactory;
        $this->logger = $logger;
    }


   
    
    public function import(Request $request, EntityManagerInterface $em)
    {
        try {
            $form = $this->formFactory->create(ExcelImportType::class);
            $form->handleRequest($request);
    
            if (!$form->isSubmitted()) {
                $this->logger->error('Formulaire non soumis');
                return $this->json(['message' => 'Formulaire non soumis'], 400);
            }
    
            if (!$form->isValid()) {
                $this->logger->error('Formulaire invalide', ['errors' => $form->getErrors()]);
                return $this->json(['message' => 'Formulaire invalide'], 400);
            }
    
            $excelFile = $form['excelFile']->getData();
    
            if (!$excelFile) {
                $this->logger->error('Fichier Excel manquant');
                return $this->json(['message' => 'Fichier Excel manquant'], 400);
            }
    
            $spreadsheet = IOFactory::load($excelFile);
            $worksheet = $spreadsheet->getActiveSheet();
            $rows = $worksheet->toArray();
        //var_dump(array_shift($rows));
            if (empty($rows) || count($rows) < 1) {
                $this->logger->error('Aucune donnée dans le fichier Excel');
                return $this->json(['message' => 'Aucune donnée dans le fichier Excel'], 400);
            }
            array_shift($rows);
            foreach ($rows as $row) {
                //var_dump($row);
              
    
                // Traitement du module
                $moduleRepository = $em->getRepository(Module::class);
                $module = $moduleRepository->findOneBy(['title' => $row[4]]);
    
                if (!$module) {
                    
                    $module = new Module();
                    $module->setTitle($row[4]);  $module->setCredit(2.1);
                    $em->persist($module);
                    $em->flush();
                }
            // Traitement de l'étudiant
            $etudiantRepository = $em->getRepository(Etudiant::class);
            $etudiant = $etudiantRepository->findOneBy(['fullName' => $row[1]]);
                        // var_dump( $etudiant);

            if (!$etudiant) {
                $etudiant = new Etudiant();
                $etudiant->setFullName($row[1]);
                $em->persist($etudiant);
                $em->flush();  // Enregistre l'étudiant pour obtenir un ID
            }
                // Création de la formation
                $formation = new Formation();
                $formation->setEtudiant($etudiant);  // Utilise l'objet étudiant (ancien ou nouveau)
                $formation->setModule($module);  // Utilise l'objet module trouvé
                $formation->setClient($row[0]);

    
                // Gestion de la date (à adapter si nécessaire)
                $date = \DateTime::createFromFormat('Y-m-d', $row[3]);
                if ($date) {
                    $formation->setDateCompletion($date);
                }
    
                $em->persist($formation);
            }
    
            $em->flush();
    
     
            return $this->json(['message' => 'Import réussi'], 200);
    
        } catch (\Exception $e) {
            $this->logger->error('Erreur lors de l\'importation', ['exception' => $e->getMessage()]);
            return $this->json(['message' => 'Échec de l\'importation', 'error' => $e->getMessage()], 400);
        }
    }
    public function importmodule(Request $request, EntityManagerInterface $em)
    {
        try {
            $form = $this->formFactory->create(ExcelImportType::class);
            $form->handleRequest($request);
    
            if (!$form->isSubmitted()) {
                $this->logger->error('Formulaire non soumis');
                return $this->json(['message' => 'Formulaire non soumis'], 400);
            }
    
            if (!$form->isValid()) {
                $this->logger->error('Formulaire invalide', ['errors' => $form->getErrors()]);
                return $this->json(['message' => 'Formulaire invalide'], 400);
            }
    
            $excelFile = $form['excelFile']->getData();
    
            if (!$excelFile) {
                $this->logger->error('Fichier Excel manquant');
                return $this->json(['message' => 'Fichier Excel manquant'], 400);
            }
    
            $spreadsheet = IOFactory::load($excelFile);
            $worksheet = $spreadsheet->getActiveSheet();
            $rows = $worksheet->toArray();
        //var_dump(array_shift($rows));
            if (empty($rows) || count($rows) < 1) {
                $this->logger->error('Aucune donnée dans le fichier Excel');
                return $this->json(['message' => 'Aucune donnée dans le fichier Excel'], 400);
            }
            array_shift($rows);
            array_shift($rows);
            array_shift($rows);
          //var_dump($rows);die();
            foreach ($rows as $row) {
                if (empty(array_filter($row)) || empty($row[0]) || empty($row[1])) {
                    continue;
                }
                if (empty(array_filter($row))) {
                    continue;
                }
               
                //var_dump($row);
                // Traitement de l'étudiant
              
    
                // Traitement du module
                $module = $em->getRepository(Module::class);
              
                $existingModule = $module->findOneBy(['title' => $row[0], 'credit' => floatval($row[1])]);
            
    
                if (!$existingModule) {
                    $module = new Module();
                    $module->setTitle($row[0]);
                    $module->setCredit(floatval($row[1]));
                    $module->setBceCredit(empty($row[2]) ? null : floatval($row[2]));
                    $module->setReCertif(empty($row[3]) ? null : floatval($row[3]));
                    $module->setBceRecert(empty($row[4]) ? null : floatval($row[4]));
                    $em->persist($module);
                    $em->flush(); 
                   // continue;  // Ici, nous passons simplement à la ligne suivante
                }  
            }
    
            $em->flush();
    
     
            return $this->json(['message' => 'Import avec success'], 200);
    
        } catch (\Exception $e) {
            $this->logger->error('Erreur lors de l\'importation', ['exception' => $e->getMessage()]);
            return $this->json(['message' => 'Échec de l\'importation', 'error' => $e->getMessage()], 400);
        }
    }

    public function getFormationsGroupedByStudent(EntityManagerInterface $em)
    {
    
    
        $query = $em->createQuery('
        SELECT e.id as etudiant_id, 
               SUM(m.credit) as total_credit, 
               f.client, 
               MONTH(f.dateCompletion) as month,  
               YEAR(f.dateCompletion) as year,
               GROUP_CONCAT(f.id) as formation_ids,
               GROUP_CONCAT(DISTINCT m.title SEPARATOR \'|\') as module_titles
             
        FROM App\Entity\Formation f
        JOIN f.etudiant e
        JOIN f.module m
        WHERE f.status IS NULL
        AND f.dateCompletion IS NOT NULL
        GROUP BY e.id, f.client, month, year
        ORDER BY f.dateCompletion DESC
    ');

    //         $groupedFormations = $query->getResult();
    //   var_dump($groupedFormations); die();
      
      $results = $query->getResult();
  // var_dump($results); die();
      // Création des attestations
      foreach ($results as $item) {
        $etudiant=$em->getRepository(Etudiant::class)->find($item['etudiant_id']);
            $attestation = new Attestation();
            $attestation->setEtudiant($etudiant);
            $attestation->setClient( $item['client']);
            $attestation->setCredit($item['total_credit']);
            $attestation->setCreatedAt(new \DateTimeImmutable());
            $attestation->setDateFormations($item['month'].'-'.$item['year']);
            $modules = explode('|', $item['module_titles']);
            $attestation->setModules($modules);
            $attestation->setStatus(false);
            $em->persist($attestation);

            $formations = explode(',', $item['formation_ids']);
                foreach ($formations as $forma) {
                    $formation = $em->getRepository(Formation::class)->find($forma);
                    if ($formation) { // Ensure the formation was fetched successfully
                        $formation->setAttestation( $attestation);
                        $formation->setStatus(true);
                    }
                }
      }
      $em->flush();
       // $attestations = $em->getRepository(Attestation::class)->findAll();
       return $this->json(['data' => 'success generation'], 200);
  }

 
}