<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Module;
use Doctrine\ORM\Tools\Pagination\Paginator;
use App\Repository\ModuleRepository;

class ModuleApiController extends AbstractController
{
   


    public function index(ModuleRepository $moduleRepository, EntityManagerInterface $em, Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1); // Default to the first page if not set
        $limit = 20; // Number of records per page. Adjust as needed.
    
        $query = $moduleRepository
            ->createQueryBuilder('m')
            ->orderBy('m.title', 'DESC') // Order by title in descending order
            ->getQuery();
    
        $paginator = new Paginator($query);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Set the offset for the query
            ->setMaxResults($limit); // Limit the number of records
    
        $data = [];
        foreach ($paginator as $module) {
            $data[] = [
                'id' => $module->getId(),
                'title' => $module->getTitle(),
                'credit' => $module->getCredit(),
                'bceCredit' => $module->getBceCredit(),
                'reCert' => $module->getReCertif(),
                'bceRecert' => $module->getBceRecert()
            ];
        }
    
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / $limit);
    
        // Include pagination meta-data if you like
        $response = [
            'modules' => $data,
            'totalItems' => $totalItems,
            'currentPage' => $page,
            'pagesCount' => $pagesCount
        ];
    
        return new JsonResponse($response);
    }
    
    
    
    
 
    public function show(int $id,EntityManagerInterface $em): JsonResponse
    {
        $module = $em->getRepository(Module::class)->find($id);
        if (!$module) {
            return new JsonResponse(['message' => 'Module not found'], Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse([
            'id' => $module->getId(),
            'title' => $module->getTitle(),
                'credit'=>$module->getCredit(),
        ]);
    }


    public function create(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $module = new Module();
        $module->setTitle($data['title']);
        $module->setCredit($data['credit']);
        $em->persist($module);
        $em->flush();
        return new JsonResponse(['message' => 'Module created'], Response::HTTP_CREATED);
    }
   
    public function update(int $id, Request $request,EntityManagerInterface $em): JsonResponse
    {
        $module = $em->getRepository(Module::class)->find($id);
        if (!$module) {
            return new JsonResponse(['message' => 'Module not found'], Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);
        $module->setTitle($data['title']);
        $module->setCredit($data['credit']);
      $em->flush();
        return new JsonResponse(['message' => 'Module updated']);
    }

   

    public function delete(int $id,EntityManagerInterface $em): JsonResponse
    {
        $module = $em->getRepository(Module::class)->find($id);
        if (!$module) {
            return new JsonResponse(['message' => 'Module not found'], Response::HTTP_NOT_FOUND);
        }

        $em->remove($module);
        $em->flush();
        return new JsonResponse(['message' => 'Module deleted']);
    }
}
