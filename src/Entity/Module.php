<?php

namespace App\Entity;

use App\Repository\ModuleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ModuleRepository::class)]
class Module
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $title = null;

    #[ORM\Column]
    private ?float $credit = null;

   

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?float $ReCertif = null;

    #[ORM\Column(nullable: true)]
    private ?float $bceCredit = null;

    #[ORM\Column(nullable: true)]
    private ?float $bceRecert = null;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getCredit(): ?float
    {
        return $this->credit;
    }

    public function setCredit(float $credit): static
    {
        $this->credit = $credit;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    
    }

    public function getReCertif(): ?float
    {
        return $this->ReCertif;
    }

    public function setReCertif(?float $ReCertif): static
    {
        $this->ReCertif = $ReCertif;

        return $this;
    }

    public function getBceCredit(): ?float
    {
        return $this->bceCredit;
    }

    public function setBceCredit(?float $bceCredit): static
    {
        $this->bceCredit = $bceCredit;

        return $this;
    }

    public function getBceRecert(): ?float
    {
        return $this->bceRecert;
    }

    public function setBceRecert(?float $bceRecert): static
    {
        $this->bceRecert = $bceRecert;

        return $this;
    }


}
