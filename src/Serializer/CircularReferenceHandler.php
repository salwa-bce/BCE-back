<?php
// src/Serializer/CircularReferenceHandler.php

namespace App\Serializer;

class CircularReferenceHandler
{
    public function __invoke($object)
    {
        // return what you want instead of the real object
        return $object->getId();
    }
}